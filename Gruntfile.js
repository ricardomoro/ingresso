module.exports = function(grunt) {
grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    clean: {
        dist: {
            src: ['dist'],
        },
    },

    exec: {
        bower: 'bower update',
        deploy: {
            cmd: function(remotePath) {
                if (remotePath) {
                    return 'rsync -avzh --delete --progress ./dist/ ' + remotePath;
                }
                return;
            }
        },
    },

    imagemin: {
        dynamic: {
            files: [{
                expand: true,
                cwd: 'img/',
                src: ['*.{png,jpg,gif}'],
                dest: 'img/',
            }],
        },
    },

    favicons: {
        options: {
            html: 'partials/favicons.php',
            HTMLPrefix: "<?php echo get_template_directory_uri(); ?>/favicons/",
            tileBlackWhite: false,
        },
        icons: {
            src: 'img/favicon.source.png',
            dest: 'favicons',
        },
    },

    less: {
        c: {
            options: {
                paths: ["less"],
            },
            files: {
                "css/ingresso.css": "less/ingresso.less",
            },
        },
    },

    postcss: {
        options: {
            map: true,
            processors: [
                require('pixrem')(), // add fallbacks for rem units
                require('autoprefixer')({browsers: '> 1%, last 2 versions'}), // add vendor prefixes
            ]
        },
        dist: {
            src: 'css/*.css'
        }
    },

    cssmin: {
        options: {
            keepSpecialComments: 0,
        },
        target: {
            files: [{
                expand: true,
                cwd: 'css',
                src: ['**/*.css', '!**/*.min.css'],
                dest: 'css',
                ext: '.min.css',
            }],
        },
    },

    uglify: {
        options: {
            mangle: false,
            compress: true
        },
        target: {
            files: [{
                expand: true,
                cwd: 'js',
                src: ['**/*.js', '!**/*.min.js'],
                dest: 'js',
                ext: '.min.js',
            }]
        }
    },

    copy: {
        dist: {
            expand: true,
            cwd: '.',
            src: ['**', '!.**', '!less/**', '!node_modules/**', '!bower.json', '!Gruntfile.js', '!package.json'],
            dest: 'dist/',
        },
    },

    watch: {
        options: {
            livereload: true,
        },
        php: {
            files: '**/*.php',
            tasks: [],
        },
        favicon: {
            files: 'img/favicon.source.png',
            tasks: ['favicons'],
        },
        less: {
            files: 'less/*.less',
            tasks: ['less'],
        },
    },
});

    // Plugins
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-exec');
    grunt.loadNpmTasks('grunt-favicons');
    grunt.loadNpmTasks('grunt-postcss');


    // Tasks
    grunt.registerTask('default', ['watch']);
    grunt.registerTask('build', [
        'clean',
        'imagemin',
        'favicons',
        'less',
        'postcss',
        'cssmin',
        'uglify'
    ]);
    grunt.registerTask('full-build', [
        'exec:bower',
        'build'
    ]);
    grunt.registerTask('dist', [
        'full-build',
        'copy'
    ]);
};
