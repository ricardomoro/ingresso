<?php get_header(); ?>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <h2>Erro 404</h2>
            <div class="alert alert-danger" role="alert">
                <p><strong>Ops!</strong> A p&aacute;gina que voc&ecirc; tentou acessar n&atilde;o existe. <a href="<?php bloginfo('url'); ?>" class="alert-link">Volte para a P&aacute;gina Inicial.</a></p>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>
