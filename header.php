<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php get_template_part('partials/favicon'); ?>

    <!-- Título -->
    <?php echo get_template_part('partials/title'); ?>

    <?php wp_head(); ?>
</head>
<body>
