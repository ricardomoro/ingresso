<?php
function load_styles() {
    /* wp_register_style( $handle, $src, $deps, $ver, $media ); */

    if (WP_DEBUG) {
        wp_register_style( 'css-ingresso', get_template_directory_uri().'/css/ingresso.css', array(), false, 'all' );
        wp_register_style( 'css-prettyPhoto', get_template_directory_uri().'/vendor/prettyPhoto/css/prettyPhoto.css', array(), false, 'screen' );
        wp_register_style( 'css-dataTables', get_template_directory_uri().'/vendor/datatables/media/css/jquery.dataTables.css', array(), false, 'screen' );
        wp_register_style( 'css-dataTables-bootstrap', get_template_directory_uri().'/vendor/datatables-bootstrap3/BS3/assets/css/datatables.css', array(), false, 'screen' );
    } else {
        wp_register_style( 'css-ingresso', get_template_directory_uri().'/css/ingresso.min.css', array(), false, 'all' );
        wp_register_style( 'css-prettyPhoto', get_template_directory_uri().'/vendor/prettyPhoto/css/prettyPhoto.css', array(), false, 'screen' );
        wp_register_style( 'css-dataTables', get_template_directory_uri().'/vendor/datatables/media/css/jquery.dataTables.min.css', array(), false, 'screen' );
        wp_register_style( 'css-dataTables-bootstrap', get_template_directory_uri().'/vendor/datatables-bootstrap3/BS3/assets/css/datatables.css', array(), false, 'screen' );
    }
}

function load_scripts() {
    if ( ! is_admin() ) {
        wp_deregister_script('jquery');
    }

    /* wp_register_script( $handle, $src, $deps, $ver, $in_footer ); */

    if (WP_DEBUG) {
        wp_register_script( 'html5shiv', get_template_directory_uri().'/vendor/html5shiv/dist/html5shiv.js', array(), false, false );
        wp_script_add_data( 'html5shiv', 'conditional', 'lt IE 9' );
        wp_register_script( 'html5shiv-print', get_template_directory_uri().'/vendor/html5shiv/dist/html5shiv-printshiv.js', array(), false, false );
        wp_script_add_data( 'html5shiv-print', 'conditional', 'lt IE 9' );

        wp_register_script( 'respond', get_template_directory_uri().'/vendor/respond-minmax/dest/respond.src.js', array(), false, false );
        wp_script_add_data( 'respond', 'conditional', 'lt IE 9' );
        wp_register_script( 'respond-matchmedia', get_template_directory_uri().'/vendor/respond-minmax/dest/respond.matchmedia.addListener.src.js', array(), false, false );
        wp_script_add_data( 'respond-matchmedia', 'conditional', 'lt IE 9' );

        wp_register_script( 'jquery', get_template_directory_uri().'/vendor/jquery/dist/jquery.js', array(), false, false );
        wp_register_script( 'bootstrap', get_template_directory_uri().'/vendor/bootstrap/dist/js/bootstrap.js', array('jquery'), false, false );
        wp_register_script( 'bootstrap-accessibility', get_template_directory_uri().'/vendor/bootstrapaccessibilityplugin/plugins/js/bootstrap-accessibility.js', array('bootstrap'), false, false );

        wp_register_script( 'jquery-prettyPhoto', get_template_directory_uri().'/vendor/prettyPhoto/js/jquery.prettyPhoto.js', array('jquery'), false, false );
        wp_register_script('prettyPhoto-config', get_template_directory_uri().'/js/prettyPhoto-config.js', array(), false, true);

        wp_register_script( 'jquery-dataTables', get_template_directory_uri().'/vendor/datatables/media/js/jquery.dataTables.js', array('jquery'), false, false );
        wp_register_script( 'jquery-dataTables-bootstrap', get_template_directory_uri().'/vendor/datatables-bootstrap3/BS3/assets/js/datatables.js', array('jquery','jquery-dataTables'), false, false );
        wp_register_script( 'dataTables-pt_BR', get_template_directory_uri().'/js/dataTables-pt_BR.js', array('jquery', 'jquery-dataTables'), false, true );
    } else {
        wp_register_script( 'html5shiv', get_template_directory_uri().'/vendor/html5shiv/dist/html5shiv.min.js', array(), false, false );
        wp_script_add_data( 'html5shiv', 'conditional', 'lt IE 9' );
        wp_register_script( 'html5shiv-print', get_template_directory_uri().'/vendor/html5shiv/dist/html5shiv-printshiv.min.js', array(), false, false );
        wp_script_add_data( 'html5shiv-print', 'conditional', 'lt IE 9' );

        wp_register_script( 'respond', get_template_directory_uri().'/vendor/respond-minmax/dest/respond.min.js', array(), false, false );
        wp_script_add_data( 'respond', 'conditional', 'lt IE 9' );
        wp_register_script( 'respond-matchmedia', get_template_directory_uri().'/vendor/respond-minmax/dest/respond.matchmedia.addListener.min.js', array(), false, false );
        wp_script_add_data( 'respond-matchmedia', 'conditional', 'lt IE 9' );

        wp_register_script( 'jquery', get_template_directory_uri().'/vendor/jquery/dist/jquery.min.js', array(), false, false );
        wp_register_script( 'bootstrap', get_template_directory_uri().'/vendor/bootstrap/dist/js/bootstrap.min.js', array('jquery'), false, false );
        wp_register_script( 'bootstrap-accessibility', get_template_directory_uri().'/vendor/bootstrapaccessibilityplugin/plugins/js/bootstrap-accessibility.min.js', array('bootstrap'), false, false );

        wp_register_script( 'jquery-prettyPhoto', get_template_directory_uri().'/vendor/prettyPhoto/js/jquery.prettyPhoto.js', array('jquery'), false, false );
        wp_register_script('prettyPhoto-config', get_template_directory_uri().'/js/prettyPhoto-config.min.js', array(), false, true);

        wp_register_script( 'jquery-dataTables', get_template_directory_uri().'/vendor/datatables/media/js/jquery.dataTables.min.js', array('jquery'), false, false );
        wp_register_script( 'jquery-dataTables-bootstrap', get_template_directory_uri().'/vendor/datatables-bootstrap3/BS3/assets/js/datatables.js', array('jquery','jquery-dataTables'), false, false );
        wp_register_script( 'dataTables-pt_BR', get_template_directory_uri().'/js/dataTables-pt_BR.min.js', array('jquery', 'jquery-dataTables'), false, true );

        wp_register_script( 'js-barra-brasil', '//barra.brasil.gov.br/barra.js', array(), false, true );
    }
}

add_action( 'wp_enqueue_scripts', 'load_styles', 1 );
add_action( 'wp_enqueue_scripts', 'load_scripts', 1 );
