<?php
add_filter('upload_mimes','restrict_mime');
function restrict_mime($mimes) {
    if (!current_user_can('administrator')) {
        $mimes = array(
            'jpg|jpeg|jpe' => 'image/jpeg',
            'png' => 'image/png',
            'gif' => 'image/gif',
        );
    }
    return $mimes;
}
