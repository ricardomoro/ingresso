<?php
// Script Condicional
require_once('inc/script_conditional.php');

// Registro de Scripts & Styles
require_once('inc/register_assets.php');

// Post Thumbnail
require_once('inc/post-thumbnails.php');

// Menu do Bootstrap
require_once('inc/wp_bootstrap_navwalker.php');

// Breadcrumb
require_once('inc/breadcrumb.php');
